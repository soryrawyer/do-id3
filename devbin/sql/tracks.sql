-- created via the supabase table creator ui
CREATE TABLE public.tracks (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    artist_name text,
    album_name text,
    title text,
    location text,
    constraint tracks_pkey primary key (id),
    constraint unique_track unique (artist_name, album_name, title, location)
);
