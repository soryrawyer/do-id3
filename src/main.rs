// read id3 tags from audio files in a digital ocean bucket
// write some metadata to postgres
use aws_sdk_s3::client::fluent_builders::ListObjectsV2;
use aws_sdk_s3::model::Object;
use aws_sdk_s3::{Client as S3Client, Config, Error};
use aws_smithy_http::endpoint::Endpoint;
use aws_types::{region::Region, Credentials};
use bytes::Buf;
use dotenv::dotenv;
use http::Uri;
use id3::{Tag, TagLike};
use openssl::ssl::{SslConnector, SslMethod};
use postgres_openssl::MakeTlsConnector;
use tokio_postgres::{Client as PgClient, Error as PgError};

use std::{env, str};

const BUCKET_NAME: &str = "media-backup";
const INSERT: &str = r#"insert into tracks (artist_name, album_name, title, location)
values ($1, $2, $3, $4)
on conflict on constraint unique_track do nothing"#;

#[derive(Debug)]
struct DbConnInfo {
    host: String,
    port: String,
    database: String,
    user: String,
    password: String,
}

impl DbConnInfo {
    fn new() -> Self {
        DbConnInfo {
            host: env::var("PGHOST").unwrap().to_owned(),
            port: env::var("PGPORT").unwrap().to_owned(),
            database: env::var("PGDATABASE").unwrap().to_owned(),
            user: env::var("PGUSER").unwrap().to_owned(),
            password: env::var("PGPASSWORD").unwrap().to_owned(),
        }
    }

    fn conn_str(&self) -> String {
        format!(
            "user={} password={} host={} port={} dbname={}",
            self.user, self.password, self.host, self.port, self.database
        )
    }
}

fn connector() -> MakeTlsConnector {
    let mut builder = SslConnector::builder(SslMethod::tls()).unwrap();
    builder.set_ca_file(env::var("PGSSLCERT").unwrap()).unwrap();
    MakeTlsConnector::new(builder.build())
}

fn get_s3_client() -> S3Client {
    let key = env::var("DO_SPACES_KEY").unwrap();
    let secret = env::var("DO_SPACES_SECRET").unwrap();

    let creds = Credentials::new(key, secret, None, None, "");
    let builder = Config::builder()
        .region(Region::new("nyc3"))
        .endpoint_resolver(Endpoint::immutable(Uri::from_static(
            "https://nyc3.digitaloceanspaces.com",
        )))
        .credentials_provider(creds);
    let config = builder.build();
    S3Client::from_conf(config)
}

#[derive(Debug)]
struct TrackInfo {
    artist_name: String,
    album_name: String,
    title: String,
    location: String,
}

impl TrackInfo {
    // create a new TrackInfo instance given ID3 tags and a block storage object key
    fn new(tags: id3::Tag, key: &String) -> Option<Self> {
        // is there a better way to get all this track info?
        let artist_name;
        if let Some(artist) = tags.artist() {
            artist_name = artist;
        } else {
            return None;
        }
        let album_name;
        if let Some(album) = tags.album() {
            album_name = album;
        } else {
            return None;
        }
        let title;
        if let Some(track_name) = tags.title() {
            title = track_name;
        } else {
            return None;
        }
        Some(TrackInfo {
            artist_name: artist_name.to_string(),
            album_name: album_name.to_string(),
            title: title.to_string(),
            location: format!("do://{}/{}", BUCKET_NAME, key),
        })
    }

    // TODO: I bet there are some URI methods that could make parsing the location
    // more robust (e.g. - handle different bucket names, different "protocols")
    fn key(&self) -> String {
        self.location.replace("do://media-backup/", "")
    }
}

async fn parse_track_info(objects: Vec<Object>, client: &S3Client) -> Vec<TrackInfo> {
    let mut result: Vec<TrackInfo> = vec![];
    for object in objects {
        let key: String = object.key().unwrap_or_default().to_owned();
        let resp = client
            .get_object()
            .range("bytes=0-2048") // 2k oughta be enough for now
            .key(&key)
            .bucket(BUCKET_NAME)
            .send()
            .await
            .unwrap();
        println!("{}", key);
        // read the bytes from out GetObject request into an ID3
        // tag reader struct and print some info
        let bs = resp.body.collect().await.unwrap();
        // if no tags, just move on
        if let Ok(tags) = Tag::read_from(bs.chunk()) {
            if let Some(track_info) = TrackInfo::new(tags, &key) {
                result.push(track_info);
            } else {
                continue;
            }
        } else {
            println!("couldn't read tags for {}", key);
            continue;
        }
    }
    result
}

// insert track data into postgres table
async fn write_tracks(client: &PgClient, tracks: &Vec<TrackInfo>) -> Result<(), PgError> {
    for track in tracks {
        client
            .execute(
                INSERT,
                &[
                    &track.artist_name,
                    &track.album_name,
                    &track.title,
                    &track.location,
                ],
            )
            .await?;
    }
    Ok(())
}

fn build_s3_call(client: &S3Client, start_after: Option<&str>) -> ListObjectsV2 {
    match start_after {
        Some(key) => client
            .list_objects_v2()
            .bucket(BUCKET_NAME)
            .max_keys(100)
            .start_after(key),
        None => client.list_objects_v2().bucket(BUCKET_NAME).max_keys(100),
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    println!("Hello, world!");
    dotenv().ok();
    let conn = DbConnInfo::new().conn_str();
    let (pg_client, connection) = tokio_postgres::connect(&conn, connector()).await.unwrap();

    // The connection object performs the actual communication with the database,
    // so spawn it off to run on its own.
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let s3_client = get_s3_client();
    let mut resp = build_s3_call(&s3_client, None).send().await?;
    loop {
        // TODO: replace this with match, put all the below junk in a function
        if let Some(contents) = resp.contents() {
            if contents.len() == 0 {
                break;
            }

            let tracks = parse_track_info(contents.to_vec(), &s3_client).await;
            if let Err(_) = write_tracks(&pg_client, &tracks).await {
                break;
            }
            let start_after = tracks.last().unwrap().key();
            println!("starting after: {:?}", start_after);

            // fetch the next round of objects from digital ocean
            resp = build_s3_call(&s3_client, Some(&start_after)).send().await?;
        } else {
            println!("all done?");
            break;
        }
    }
    Ok(())
}
