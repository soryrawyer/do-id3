// test out some postgres stuff

use std::env;

use dotenv::dotenv;
use openssl::ssl::{SslConnector, SslMethod};
use postgres::types::FromSql;
use postgres::Client;
use postgres_openssl::MakeTlsConnector;
use uuid::Uuid;

#[derive(Debug)]
struct DbConnInfo {
    host: String,
    port: String,
    database: String,
    user: String,
    password: String,
}

impl DbConnInfo {
    fn conn_str(&self) -> String {
        format!(
            "user={} password={} host={} port={} dbname={}",
            self.user, self.password, self.host, self.port, self.database
        )
    }
}

// read connection info from pgpass
fn db_info() -> DbConnInfo {
    DbConnInfo {
        host: env::var("PGHOST").unwrap().to_owned(),
        port: env::var("PGPORT").unwrap().to_owned(),
        database: env::var("PGDATABASE").unwrap().to_owned(),
        user: env::var("PGUSER").unwrap().to_owned(),
        password: env::var("PGPASSWORD").unwrap().to_owned(),
    }
}

fn main() {
    println!("hi");
    dotenv().ok();
    let conn_str = db_info().conn_str();
    let mut builder = SslConnector::builder(SslMethod::tls()).unwrap();
    builder.set_ca_file(env::var("PGSSLCERT").unwrap()).unwrap();
    let connector = MakeTlsConnector::new(builder.build());
    let mut client = Client::connect(&conn_str, connector).unwrap();
    for row in client
        .query("select id, title, album_name, artist_name from tracks", &[])
        .unwrap()
    {
        let id: Uuid = row.get(0);
        let title: String = row.get(1);
        let album: String = row.get(2);
        let artist: String = row.get(3);
        println!("{:?} {:?} {:?} {:?}", id, title, album, artist);
    }

    client
        .execute(
            "insert into tracks (artist_name, album_name, title, location) values ($1, $2, $3, $4) on conflict on constraint unique_track do nothing",
            &[
                &"test artist".to_string(),
                &"test album".to_string(),
                &"test track title".to_string(),
                &"do://media-backup/audio/test artist/test album/test track title".to_string(),
            ],
        )
        .unwrap();
}
